(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('profileController', profileController);
    profileController.$inject = ['$scope', 'atletaCursaFactory','userFactory','$window','$timeout'];
    function profileController($scope, atletaCursaFactory, userFactory,$window,$timeout) {
        $scope.env={
            saving:false,
            model:{},
            password:{}
        };

        userFactory.auth( function(user){
            $scope.env.user = user;
            $scope.env.model = user.attributes;
            $scope.$apply();
        });

        $scope.save = function(data){
            $scope.env.saving = true;
            $scope.env.user.update(data, function(result){
                $scope.env.saving = false;
            })
        };

        $scope.changePassword = function(pass1, pass2){
            if ( pass1==pass2 ){
                $scope.env.saving = true;
                $scope.env.user.update({password:pass1}, function(result){
                    $scope.env.saving = false;
                })
            }
        }


    }
})(angular);