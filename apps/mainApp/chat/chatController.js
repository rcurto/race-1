(function() {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('chatController', chatController);

    chatController.$inject=['$scope', 'userFactory'];

    function chatController($scope,  userFactory) {

        $scope.sessionSeverTocken = "80fd8a93551b9cc5ddb79b6380962c138cffae15";
        $scope.env = {
            user: null,
            displayMessage: null
        };
        userFactory.auth( function(user){
            $scope.env.user = user;

            if(user.photo)
                $scope.iframeSrcUrl = 'apps/chatApp/index.php?nickname=' + user.username +"&sessionservertocken=" + $scope.sessionSeverTocken +"&photo_url=" + user.photo.url();
            else
                $scope.iframeSrcUrl = 'apps/chatApp/index.php?nickname=' + user.username +"&sessionservertocken=" + $scope.sessionSeverTocken;

            $scope.$apply();
            
            user.getDisplayMessage( function(result){
                $scope.env.displayMessage = result.get( $scope.env.user.get('language') );
                $scope.$apply()
            });
        });
    }
})();
