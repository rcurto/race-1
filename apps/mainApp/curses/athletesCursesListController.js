(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('athletesCursesListController', athletesCursesListController);

    athletesCursesListController.$inject = ['$scope', 'cursesFactory', '$filter'];
    function athletesCursesListController($scope, cursesFactory, $filter) {

        $scope.env = {
            loading: true,
            posts: [],
            postsSource: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            filters: ''
        };
        $scope.remove = function remove(post ,index) {
            var confirmAction = confirm('Delete?');
            if ( !confirmAction ){
                return;
            }
            $scope.env.postsSource = $scope.env.postsSource.filter(function (row) {
                if (row.getId() != post.getId()) {
                    return true;
                }
                return false;
            });
            $scope.env.posts.splice(index,1);
            post.delete();
        };



        cursesFactory.getAll(function (posts) {
            $scope.env.loading = false;
            $scope.env.posts = posts;
            $scope.env.postsSource = posts;
            $scope.$apply()
        });




    }
})(angular);