(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('cursesListController', cursesListController);

    cursesListController.$inject = ['$scope', 'cursesFactory', 'userFactory', '$filter', 'circuitsFactory', 'countryFactory'];
    function cursesListController($scope, cursesFactory, userFactory, $filter, circuitsFactory, countryFactory) {

        $scope.env = {
            loading: true,
            posts: [],
            postsSource: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            filters: {},
            joins: [],
            user: null,
            circuits: []

        };
        $scope.config = {
            itemsPerPage: 50,
            fillLastPage: true
        };

        $scope.remove = function remove(post, index) {
            var confirmAction = confirm('Delete?');
            if (!confirmAction) {
                return;
            }
            $scope.env.postsSource = $scope.env.postsSource.filter(function (row) {
                if (row.getId() != post.getId()) {
                    return true;
                }
                return false;
            });
            $scope.env.posts.splice(index, 1);
            post.delete();
        };

        circuitsFactory.getAll(function (posts) {
            $scope.env.circuits = posts;
            $scope.$apply()
        });

        countryFactory.getAll(function (posts) {
            $scope.env.country = posts;
            $scope.$apply()
        });


        cursesFactory.getAll(function (posts) {
            $scope.env.loading = false;
            $scope.env.posts = posts;
            $scope.env.postsSource = posts;
            $scope.$apply()
        });

        userFactory.auth(function (user) {
            $scope.env.user = user;
            if (user != null) {
                user.getJoinCoursesIds(function (ids) {
                    $scope.env.joins = ids;
                    $scope.$apply()
                })
            }
        });


        $scope.$watchCollection('env.filters', function () {

            var data = null;
            var country_id = null;
            var zone_id = null;
            var circuit_id = null;
            var categoria = $scope.env.filters.categoria ? $scope.env.filters.categoria : null;

            if ($scope.env.filters.country) {
                country_id = $scope.env.filters.country.getId();
            }
            if ($scope.env.filters.zone) {
                zone_id = $scope.env.filters.zone.id;
            }
            if ($scope.env.filters.circuit) {
                circuit_id = $scope.env.filters.circuit.getId();
            }
            if ($scope.env.filters.printData) {
                data = $filter("date")($scope.env.filters.printData, 'dd-MM-yyyy');
            }

            $scope.env.posts = $scope.env.postsSource.filter(function (value) {

                if (country_id != null && value.countryId != country_id) {
                    return false;
                }
                if (zone_id != null && value.zoneId != zone_id) {
                    return false;
                }

                if (data != null && value.printData.indexOf(data) == -1) {
                    return false;
                }

                if (categoria != null && value.categoria != categoria) {
                    return false;
                }
                if (circuit_id != null && value.circuitId != circuit_id) {
                    return false;
                }
                return true;
            })


            //$scope.env.posts = $filter("filter")($scope.env.postsSource, $scope.env.filters);
        });

    }
})(angular);