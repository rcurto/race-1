(function() {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('myGoalsController', myGoalsController);

    myGoalsController.$inject=['$scope', 'userFactory','atletaCursaFactory', 'cursesFactory', 'feedbackFactory', 'eventFactory'];

    function myGoalsController($scope,  userFactory, atletaCursaFactory, cursesFactory, feedbackFactory, eventFactory) {

		$scope.sessionSeverTocken = "80fd8a93551b9cc5ddb79b6380962c138cffae15";
        $scope.env = {
            loading: true,
            nextPrincipalRace: null,
            displayMessage: null,
            nextSecondaryRace: null,
            feedback:[],
            feedbackAlter:false,


            user: null,
            lastRaceResult:[],
            lastNewRace:[],
            topRecords:[],
            topRaces:[],
            topRunningShoes:[],
            coachCommentWeeklyAlert:false,
            coachCommentRaceAlert:false,
            missingRaceFeedback:null,
            weeklyFeedback: undefined,
            lastRaceFeedback: undefined,
            events:[]
        };

        $scope.$parent.initPage = init; // start point
        var promises = [];
        $scope.user = null;
        function init() {
            var user = $scope.$parent.user;
            $scope.user = user;

            /**
             * get a next PRINCIPAL RACE for athlete
             */
            var nextPrincipalRacePromise = user.getNextPrincipalRace().then(function(result){
                $scope.env.nextPrincipalRace = result;
            });
            promises.push(nextPrincipalRacePromise);

            var secondaryRacePromise = user.getNextSecondaryRace().then(function(result){
                $scope.env.nextSecondaryRace = result;
            });
            promises.push(secondaryRacePromise);


            /**
             * message to the athlete
             */
            var messagePromise = user.getDisplayMessage().then(function(result){
                $scope.env.displayMessage = result.get( user.get('language') );
            });
            promises.push(messagePromise);




            if ( user.isAthlete() ){

                var FeedbackPromise = user.getFeedback().then(function(feedback){
                    $scope.env.feedback = feedback;
                    angular.forEach(feedback, function(row){
                        if(row.get("coachFeedback") == true && row.get("flgUserView") == false){
                            $scope.env.coachCommentWeeklyAlert = true;
                        }
                        if ( row.get('fullFeedback')!=undefined ){
                            return;
                        }
                        if (  moment().isAfter( moment( row.get('startDate') ).add(3,'days') ) ){
                            $scope.env.feedbackAlter = true;
                        }
                    });
                    $scope.$apply();
                });
                promises.push(FeedbackPromise);


                var weeklyFeedbackPromise = user.getWeeklyFeedback().then(function(feedback){
                    $scope.env.weeklyFeedback = feedback;
                });

                promises.push(weeklyFeedbackPromise);

                user.getLastRaceFeedback( function(feedback){
                    $scope.env.lastRaceFeedback = feedback;
                    $scope.$apply();
                });

            }else{
                feedbackFactory.getFeedbackWitOutCoachComment( function(feedbacks){
                    $scope.env.feedback = feedbacks;
                    $scope.$apply();
                })
            }


            $scope.loadLastRace();

            user.getRaces( function(races){
                angular.forEach(races, function(race){
                    if(race.get("isCoachFull") == true && race.get("flgUserView") == false){
                        $scope.env.coachCommentRaceAlert = true;
                    }
                    if ( race.get('isFull')!=undefined ){
                        return;
                    }
                    if (  moment().startOf('isoWeek').isAfter( moment( race.nomcursa.data,'YYYY-MM-DD' ) ) ){
                        $scope.env.missingRaceFeedback =  race.nomcursa.nom;
                    }
                });
                $scope.$apply()
            });


            atletaCursaFactory.getLastRaceResult(10, undefined, function(result){
                $scope.env.lastRaceResult = result;
                $scope.$apply()
            });

            cursesFactory.topRaces(10, function(races){
                $scope.env.topRaces = races;
                $scope.$apply()
            });

            eventFactory.getJoinedRaceEvents(15).then(function(events){
                $scope.env.events = events;
                $scope.$apply()
            });






            Parse.Promise.when(promises).then(function () {
                pageLoaded();
            });
        }

        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }


        $scope.loadLastRace = function(race){
            if ( race ){
                race.join(race.objective, $scope.user, function(){
                    $scope.user.getRaceWithOutJoin(10, function(result){
                        $scope.env.lastNewRace = result;
                        $scope.$apply()
                    })
                });
            }else{
                $scope.user.getRaceWithOutJoin(10, function(result){
                    $scope.env.lastNewRace = result;
                    $scope.$apply()
                })
            }
        };



    }

})();