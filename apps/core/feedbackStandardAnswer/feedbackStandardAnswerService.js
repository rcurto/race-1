(function (angular) {
    'use strict';
    angular.module('core').service('feedbackStandardAnswerService', feedbackStandardAnswerService);

    function feedbackStandardAnswerService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);

            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            return object;
        }

    }
})(angular);

