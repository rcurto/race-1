(function (angular) {
    'use strict';
    angular.module('core').service('feedbackFactory', feedbackFactory);

    feedbackFactory.$inject = ['feedbackService'];

    function feedbackFactory(feedbackService) {
        var cache = [];
        return {
            getById: getById,
            getByUser: getByUser,
            getWeeklyFeedbackByUser: getWeeklyFeedbackByUser,
            getFeedbackWitOutCoachComment: getFeedbackWitOutCoachComment,
            store: store
        };


        function store(data, callback) {
            var parseObject = Parse.Object.extend("Feedback");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }
            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("Feedback");
            var query = new Parse.Query(parseObject);
            query.include('user');

            query.get(id, {
                success: function (results) {
                    callback(feedbackService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getFeedbackWitOutCoachComment(callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Feedback");
            var query = new Parse.Query(parseObject);
            query.include('user');
            query.equalTo('fullFeedback', true);
            query.equalTo('coachComment', null);
            query.equalTo('answer', null);
            query.descending('startDate');
           // query.descending('startDate');
            query.limit(1000);
            query.find({
                success: function (results) {
                    cache = results;
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(feedbackService(row));
                    });
                    if (callback){
                        callback(rows);
                    }
                    promise.resolve(rows);

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }

        function getByUser(user) {
            var promise = new Parse.Promise();
            var parseObject = Parse.Object.extend("Feedback");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.include('user');
            query.include('answer');
            query.descending('startDate');
            query.limit(3);
            query.find({
                success: function (results) {
                    cache = results;
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(feedbackService(row));
                    });
                    promise.resolve(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }

        function getWeeklyFeedbackByUser(user) {
            var promise = new Parse.Promise();

            var now = moment().utc().toDate();
            var parseObject = Parse.Object.extend("Feedback");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.equalTo('coachFeedback', true);

            query.lessThanOrEqualTo('startDate', now);
            query.greaterThanOrEqualTo('endDate', now);
            query.include('user');
            query.descending('startDate');
            query.first({
                success: function (result) {
                    promise.resolve(result ? feedbackService(result) : null);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }

    }
})(angular);

