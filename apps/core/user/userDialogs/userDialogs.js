(function (angular) {
    'use strict';


    function userDialogsDirective() {
        return {
            templateUrl: 'apps/core/user/userDialogs/userDialogs.html',
            //ngModel: 'require',
            restrict: 'E',
            controller: userDialogsController,
            scope: {
                show: '=',
                form: '@',
            }

        };
        userDialogsController.$inject = ['$scope', 'userFactory', '$state', '$location'];

        function userDialogsController($scope, userFactory, $state, $location) {
            $scope.env = {

                login:{
                    data:{
                        email: null,
                        password: null
                    },
                    error: null,
                    waiting: false
                },

                errors: {
                    login:null,
                    register:null,
                    forgot:null
                },

                register:{

                },
                forgot:{
                    email:null
                },
                currentLocation: null
            };

            $scope.$watch(function(){return $state.current.url},function(value){
                $scope.env.currentLocation = value;
            });

            $scope.$watch('show', function(){
                if ( $scope.show==true ){
                    $('#myModal').modal('show');
                    $scope.show=false;
                }
            });


            $scope.login = function(data) {
                $scope.env.login.waiting = true;

                userFactory.login(data.email, data.password, function(answer){
                    if ( answer.success==true ){
                        $('#myModal').modal('hide');
                        $('div.modal-backdrop').hide();
                        $state.reload(true);
                    }else{
                        $scope.env.waiting = false;
                        $scope.env.login.error = answer.error
                    }
                    $scope.$apply()
                })

            };

            $scope.forgot = function() {
                $scope.env.waiting = true;
                var credentials = {
                    email: $scope.env.login.email,
                };

                userFactory.forgot(credentials,function(data){
                    if ( data.success==true ){
                        $state.reload(true)
                        $('#myModal_'+$scope.id).modal('hide');
                    }else{
                        $scope.env.waiting = false;
                        $scope.env.errors.forgot = data.error
                    }
                })

            };

            $scope.register = function() {
                $scope.env.waiting = true;
                var data = $scope.env.register;

                userFactory.register(data,function(answer){
                    if ( answer.success==true ){
                        userFactory.login(data,function(answer){
                            if ( answer.success==true ) {
                                $state.reload(true)
                                $('#myModal_'+$scope.id).modal('hide');
                            }else{
                                $scope.env.errors.register = answer.error
                            }
                        })
                    }else{
                        $scope.env.errors.register = answer.error
                    }

                    $scope.env.waiting = false;

                })
            }

        }


    }

    angular.module('core').directive('userDialogs', userDialogsDirective);


})(window.angular);
